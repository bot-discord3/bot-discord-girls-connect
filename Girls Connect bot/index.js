//------------- appelle d'outil pour le bot -------------

const { Client, GatewayIntentBits, EmbedBuilder, ApplicationCommandOptionChannelTypesMixin} = require("discord.js");
const fs = require('fs');
const {AuteurDuBot, prefix, IdBot, token, dossierEventJson, dossierHeroesJson, idChannelEvent} = require("./confConst.js");
let jsonFileHero = require(dossierHeroesJson);
let jsonFileEvent = require(dossierEventJson);

//------------- creation de fonction -------------

function isKeyPresent(heros, key) {
    const keys = Object.keys(heros);
    return keys.includes(key);
}

function readJSONFileHero() {
    try {
      const jsonFileHero = fs.readFileSync(dossierHeroesJson, 'utf-8');
      let heros = JSON.parse(jsonFileHero);
      console.log("json Hero inissialisé")
      return heros;
    } catch (err) {
      console.error(err);
    }
}

function readJSONFileEvent() {
    try {
      const readJSONFileEvent = fs.readFileSync(dossierEventJson, 'utf-8');
      let heros = JSON.parse(readJSONFileEvent);
      console.log("json Event inissialisé")
      return heros;
    } catch (err) {
      console.error(err);
    }
}

function donnaisDateActuelle(){
    dateActuelle = new Date();
    return dateActuelle.getMonth() + 1 + "/" + dateActuelle.getDay() + " " + dateActuelle.getHours() + ":" + dateActuelle.getMinutes() + ":" + dateActuelle.getSeconds();
}

function resetDateActuelle(){
    dateActuelle = new Date();
}

//------------- définition des variables -------------

const heros = readJSONFileHero();
const event = readJSONFileEvent();
let buffer;
let timer_unTimer = 0;
let timer_pingPing = 0;

let tryPing;

let switchMessage = true;

let tresJolieMessage;
let I = 0;

let dateActuelle = new Date();

let miseEnPlaceEvent;
let messageEvent;

const client = new Client({
    intents : [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.MessageContent
    ]
});

//------------- dit si la connection est reussite -------------

client.on("ready", () => {
    console.log("bot opérationnel");
});

//------------- creation de test -------------



//------------- ouverture de la récuperation des messages, et lancement des testes -------------

client.on("messageCreate", message => {

//------------- creation de test de message -------------



//------------- creation des fonctions utilisant message -------------
    function unTimer(){
        message.reply("le timer dure depuis " + timer_unTimer + " seconde");
        timer_unTimer++;

    }

    function pingPong(){

        if (switchMessage){
            tryPing.edit("Ping for " + timer_pingPing + " second");
            switchMessage = false;
        } else {
            message.edit("Pong for " + timer_pingPing + " second");
            switchMessage = true;
        }
        timer_pingPing++;
    }
        
    function tresjolie(){

        if (I == 0){
            tresJolieMessage.edit("----------" + donnaisDateActuelle());
            I++;
        }else if (I == 1){
            tresJolieMessage.edit("|---------" + donnaisDateActuelle());
            I++;
        }else if (I == 2){
            tresJolieMessage.edit("-|--------" + donnaisDateActuelle());
            I++;
        }else if (I == 3){
            tresJolieMessage.edit("--|-------" + donnaisDateActuelle());
            I++;
        }else if (I == 4){
            tresJolieMessage.edit("---|------" + donnaisDateActuelle());
            I++;
        }else if (I == 5){
            tresJolieMessage.edit("----|-----" + donnaisDateActuelle());
            I++;
        }else if (I == 6){
            tresJolieMessage.edit("-----|----" + donnaisDateActuelle());
            I++;
        }else if (I == 7){
            tresJolieMessage.edit("------|---" + donnaisDateActuelle());
            I++;
        }else if (I == 8){
            tresJolieMessage.edit("-------|--" + donnaisDateActuelle());
            I++;
        }else if (I == 9){
            tresJolieMessage.edit("--------|-" + donnaisDateActuelle());
            I++;
        }else if (I == 10){
            tresJolieMessage.edit("---------|" + donnaisDateActuelle());
            I = 0;
        }else{
            console.log("ya un putin de probleme de merde");
        }


    }

    function affichageEvent(){

        resetDateActuelle();

        const embedEvent = new EmbedBuilder()
                .setColor(event[miseEnPlaceEvent].color)
                .setTitle("event : " + event[miseEnPlaceEvent].name)
                .setAuthor(
                    {name: 'bot created by ' + AuteurDuBot}
                )
                .setDescription(event[miseEnPlaceEvent].description)
                .setThumbnail(event[miseEnPlaceEvent].icone)
                .addFields(
                    { 
                        name: "__Event start in :__",
                        value: calculeTempRestant(event[miseEnPlaceEvent])
                    },
                )
                .setTimestamp()
                .setFooter(
                    { text : event[miseEnPlaceEvent].footer}
                )
            
            messageEvent.edit({ embeds: [embedEvent] });

    }

    function calculeTempRestant(jsonEvent){

        let tempsHeur = "24" - dateActuelle.getHours();
        console.log(tempsHeur);
        tempsHeur = parseInt(tempsHeur) + parseInt(jsonEvent.heurDebut);
        console.log(jsonEvent.heurDebut);
        console.log(tempsHeur);
        if (tempsHeur >= 24){
            tempsHeur = tempsHeur - 24;
            console.log(tempsHeur);
        }

        let tempsMinute = dateActuelle.getMinutes();
        if (tempsMinute > 0){
            tempsHeur = tempsHeur - 1;
            console.log(tempsHeur);
            tempsMinute = 60 - tempsMinute;
        }

        let tempsSeconde = dateActuelle.getSeconds();
        if(tempsSeconde > 0){
            tempsMinute = tempsMinute -1;
            tempsSeconde = 60 - tempsSeconde;
            if (tempsMinute < 0){
                tempsHeur = tempsHeur -1;
                console.log(tempsHeur);
                tempsMinute = 59;
            }
        }

        console.log(tempsHeur + "   |   " + tempsMinute + "   |   " + tempsSeconde);

        return tempsHeur + ":" + tempsMinute + ":" + tempsSeconde;

    }

//------------- modification du message ressus -------------


    buffer = message.content;
    if (buffer.slice(0,1) === prefix){
        buffer = buffer.substring(1);

//------------- help -------------

        if(buffer === "help"){
            message.reply("in progress");
        }

//------------- repond a des choses inutile mais drole -------------

        else if(buffer === "Petrolia best girl"){

            message.reply("nop, it's M. Kuzunoha");
    
        } else if(buffer === "lamention"){
    
            message.reply("Mention d'un utilisateur : <@" + message.author.id + "> \n Mention d'un salon : <#" + message.channel.id + ">");
    
        } 

//------------- information de base des heros -------------
        
        else if(isKeyPresent(heros,buffer)){

            
            const embedHero = new EmbedBuilder()
                .setColor(heros[buffer].element)
                .setTitle(heros[buffer].name)
                .setAuthor(
                    {name: 'bot created by ' + AuteurDuBot}
                )
                .setDescription(heros[buffer].description)
                .setThumbnail(heros[buffer].icone)
                .addFields(
                    { 
                        name: heros[buffer].unique_skill_name,
                        value: heros[buffer].unique_skill_content ,inline: true
                    },
                    {
                        name: heros[buffer].ultimate_name,
                        value: heros[buffer].ultimate_content ,inline: true
                    },
                    { name: '\u200B', value: '\u200B' },
                    {
                        name: heros[buffer].passive1_name,
                        value: heros[buffer].passive1_content ,inline: true
                    },
                    {
                        name : heros[buffer].passive2_name,
                        value : heros[buffer].passive2_content ,inline: true
                    },
                )
                .setImage(heros[buffer].full_art)
                .setTimestamp()
                .setFooter(
                    { text : heros[buffer].footer}
                )
            
            message.channel.send({ embeds: [embedHero] });

        }

//------------- Event -------------

        else if(isKeyPresent(event,buffer) && message.channel == idChannelEvent){

            miseEnPlaceEvent = buffer;
            message.channel.send(".lancement de l'evenement " + miseEnPlaceEvent);
            return;

        }

        if(buffer == "lancement de l'evenement " + miseEnPlaceEvent && message.author.id == IdBot){

            messageEvent = message;
            messageEvent.edit("started")
                setInterval(affichageEvent,1000);
            
        }

//------------- teste -------------

        else if (buffer === "ping admin"){

            message.channel.send("yes <@&1084490291572121741>");

        } 
        
        else if(buffer === "timer"){

            setInterval(unTimer,2000);

        }

        else if(buffer === "start ping pong"){

            setInterval(pingPong,1000);

        }

        else if (buffer === "test admin"){

            if (message.member.roles.cache.has('1084490291572121741')) {
                message.reply('User is an admin.');
            } else {
                message.reply("not an administrator");
            }

        }

        else if(buffer == "test"){
            message.channel.send(".bonjours");
        }

        else if(buffer == "bonjours" && message.author.id == IdBot){
            tryPing = message;
            setInterval(pingPong,1000);
        }

        else if(buffer == "testJoli"){
            message.channel.send(".joliBonjour");
        }

        else if(buffer == "joliBonjour" && message.author.id == IdBot){
            tresJolieMessage = message;
            setInterval(tresjolie,1000);
        }


//------------- erreur de demande de la part de l'utilisateur -------------

        else if(message.author.id == IdBot){
            return;
        }

        else{

            message.reply("I don't understand what you want\n writes \".help\" for more information");

        }
    }
});


//------------- token de connection -------------

client.login(token);